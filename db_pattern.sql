-- MySQL dump 10.13  Distrib 5.7.32, for Linux (x86_64)
--
-- Host: localhost    Database: amd
-- ------------------------------------------------------
-- Server version	5.7.32-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `amd`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `amd` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `amd`;

--
-- Table structure for table `amd_settings`
--

DROP TABLE IF EXISTS `amd_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amd_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT 'default',
  `initialSilence` int(11) DEFAULT NULL COMMENT 'Is maximum initial silence duration before greeting. If this is exceeded set as MACHINE',
  `greeting` int(11) DEFAULT NULL COMMENT 'is the maximum length of a greeting.\n                  If this is exceeded set as MACHINE',
  `afterGreetingSilence` int(11) DEFAULT NULL COMMENT 'Is the silence after detecting a greeting.\n                  If this is exceeded set as HUMAN',
  `totalAnalysis` int(11) DEFAULT NULL COMMENT 'Is the maximum time allowed for the algorithm\n                  to decide HUMAN or MACHINE',
  `miniumWordLength` int(11) DEFAULT NULL COMMENT 'Is the minimum duration of Voice considered to be a word',
  `betweenWordSilence` int(11) DEFAULT NULL COMMENT 'Is the minimum duration of silence after a word to consider the audio that follows to be a new word',
  `maximumNumberOfWords` int(11) DEFAULT NULL COMMENT 'Is the maximum number of words in a greeting\n                  If this is exceeded set as MACHINE',
  `silenceThreshold` int(11) DEFAULT NULL COMMENT 'How long do we consider silence',
  `maximumWordLength` int(11) DEFAULT NULL COMMENT 'Is the maximum duration of a word to accept.\n                  If exceeded set as MACHINE',
  `active` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amd_settings`
--

LOCK TABLES `amd_settings` WRITE;
/*!40000 ALTER TABLE `amd_settings` DISABLE KEYS */;
INSERT INTO `amd_settings` (`id`, `name`, `initialSilence`, `greeting`, `afterGreetingSilence`, `totalAnalysis`, `miniumWordLength`, `betweenWordSilence`, `maximumNumberOfWords`, `silenceThreshold`, `maximumWordLength`, `active`) VALUES (1,'default',2500,1500,800,5000,100,50,3,256,5000,0);
/*!40000 ALTER TABLE `amd_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amd_table`
--

DROP TABLE IF EXISTS `amd_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amd_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniqueid` varchar(45) DEFAULT NULL,
  `calldate` varchar(40) DEFAULT NULL,
  `amdstatus` varchar(40) DEFAULT NULL,
  `amdcause` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amd_table`
--

LOCK TABLES `amd_table` WRITE;
/*!40000 ALTER TABLE `amd_table` DISABLE KEYS */;
INSERT INTO `amd_table` (`id`, `uniqueid`, `calldate`, `amdstatus`, `amdcause`) VALUES (6,'834933067','2021-05-03 13:13:31','HANGUP','MAXWORDLENGTH'),(7,'374519997','2021-05-03 13:14:26','NOTSURE','MAXWORDLENGTH'),(8,'39911061','2021-05-03 13:14:42','HUMAN','MAXWORDLENGTH'),(9,'384693987','2021-05-03 13:14:48','MACHINE','MAXWORDLENGTH'),(10,'463843559','2021-05-03 13:15:12','MACHINE','TOOLONG'),(11,'540298122','2021-05-03 13:15:44','MACHINE','INITIALSILENCE'),(12,'74202064','2021-05-03 14:44:55','MACHINE','INITIALSILENCE'),(13,'177748268','2021-05-03 14:45:27','MACHINE','INITIALSILENCE'),(14,'877659090','2021-05-03 14:47:02','MACHINE','INITIALSILENCE'),(15,'770818650','2021-05-03 14:48:28','MACHINE','INITIALSILENCE'),(16,'146615866','2021-05-03 21:28:05','MACHINE','INITIALSILENCE'),(17,'1620059468.6','2021-05-03 21:31:16','MACHINE',''),(18,'1620059589.9','2021-05-03 21:33:17','MACHINE',''),(19,'1620059974.12','2021-05-03 21:39:42','MACHINE',''),(20,'1620059976.15','2021-05-03 21:39:44','MACHINE',''),(21,'1620060024.18','2021-05-03 21:40:32','MACHINE',''),(22,'1620106829.0','2021-05-04 10:40:37','MACHINE','MAXWORDS-6-5'),(23,'1620106882.3','2021-05-04 10:41:32','HUMAN','HUMAN-2260-2250'),(24,'1620106902.6','2021-05-04 10:41:50','MACHINE','MAXWORDS-6-5'),(25,'1620106918.9','2021-05-04 10:42:07','HUMAN','HUMAN-2260-2250'),(26,'1620106933.12','2021-05-04 10:42:22','HUMAN','HUMAN-2260-2250'),(27,'1620106964.15','2021-05-04 10:42:50','MACHINE','MAXWORDS-6-5'),(28,'1620106976.18','2021-05-04 10:43:02','HUMAN','HUMAN-2260-2250'),(29,'1620107063.21','2021-05-04 10:44:31','HUMAN','HUMAN-2260-2250'),(30,'1620107087.24','2021-05-04 10:44:59','HUMAN','HUMAN-2260-2250'),(31,'1620107141.27','2021-05-04 10:45:58','MACHINE','MAXWORDS-6-5'),(32,'1620107181.30','2021-05-04 10:46:30','MACHINE','MAXWORDS-6-5'),(33,'1620237808.4','2021-05-05 23:03:28','UNDEF','UNDEF'),(34,'1620237808.5','2021-05-05 23:03:28','UNDEF','UNDEF'),(35,'1620239096.141','2021-05-05 23:25:04','MACHINE','MAXWORDS-6-5'),(36,'1620241415.169','2021-05-06 00:03:46','MACHINE','MAXWORDS-6-5'),(37,'1620241506.182','2021-05-06 00:05:17','MACHINE','MAXWORDS-6-5'),(38,'1620241559.192','2021-05-06 00:06:10','MACHINE','MAXWORDS-6-5'),(39,'1620241577.202','2021-05-06 00:06:29','MACHINE','MAXWORDS-6-5'),(40,'1620241680.207','2021-05-06 00:08:12','MACHINE','MAXWORDS-6-5'),(41,'1620241835.212','2021-05-06 00:10:46','MACHINE','MAXWORDS-6-5'),(42,'1620241853.217','2021-05-06 00:11:04','MACHINE','MAXWORDS-6-5'),(43,'1620242242.267','2021-05-06 00:17:33','MACHINE','MAXWORDS-6-5'),(44,'1620242265.277','2021-05-06 00:17:56','MACHINE','MAXWORDS-6-5'),(45,'1620242771.312','2021-05-06 00:26:22','MACHINE','MAXWORDS-6-5'),(46,'1620291422.102143','2021-05-06 13:57:10','MACHINE','MAXWORDS-6-5'),(47,'1620291482.102163','2021-05-06 13:58:11','MACHINE','MAXWORDS-6-5'),(48,'1620293855.102991','2021-05-06 14:37:41','MACHINE','MAXWORDS-6-5'),(49,'1620293886.103011','2021-05-06 14:38:12','MACHINE','MAXWORDS-6-5'),(50,'1620294533.103241','2021-05-06 14:49:11','MACHINE','INITIALSILENCE-4000-4000');
/*!40000 ALTER TABLE `amd_table` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-06 14:51:10
