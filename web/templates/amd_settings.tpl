<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <h1 class="h2">AMD Reports</h1>
      <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
          <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
          <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
        </div>
        <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
          This week
        </button>
      </div>
    </div>
    <form id ="launch_edit_profile_form" method="POST" >
      <input type="hidden" id = "edit_profile_id" name="edit_profile_id" value="0">
    </form>

    <form id ="edit_profile_form" method="POST" action="index.php?main_page=set_profile"></form>

    <div id = "show_export" style="display: none;">
      <textarea  type="text"  id = "show_export_input" cols="40" rows="11"></textarea>
    </div>
    <div class="form-group" id ="edit_profile_form_block"style="display: {$show_edit_form}">
      <div class="col-5">
        <h3>AMD Edit {$profile.name}</h3>
                  
                  <input  form = "edit_profile_form" class="form-control" type="hidden" name="set_profile[id]" value="{$profile.id}"></input>
                  initialSilence<input  form = "edit_profile_form" class="form-control" type="text" name="set_profile[initialSilence]" value="{$profile.initialSilence}"></input>
                  greeting<input  form = "edit_profile_form" class="form-control" type="text" name="set_profile[greeting]"        value="{$profile.greeting}"></input>
                  afterGreetingSilence<input  form = "edit_profile_form" class="form-control" type="text" name="set_profile[afterGreetingSilence]" value="{$profile.afterGreetingSilence}"></input>
                  totalAnalysis<input  form = "edit_profile_form" class="form-control" type="text" name="set_profile[totalAnalysis]" value="{$profile.totalAnalysis}"></input>
                  miniumWordLength<input  form = "edit_profile_form" class="form-control" type="text" name="set_profile[miniumWordLength]" value="{$profile.miniumWordLength}"></input>
                  betweenWordSilence<input  form = "edit_profile_form" class="form-control" type="text" name="set_profile[betweenWordSilence]" value="{$profile.betweenWordSilence}"></input>
                  maximumNumberOfWords<input  form = "edit_profile_form" class="form-control" type="text" name="set_profile[maximumNumberOfWords]" value="{$profile.maximumNumberOfWords}"></input>
                  silenceThreshold<input  form = "edit_profile_form" class="form-control" type="text" name="set_profile[silenceThreshold]" value="{$profile.silenceThreshold}"></input>
                  maximumWordLength<input  form = "edit_profile_form" class="form-control" type="text" name="set_profile[maximumWordLength]" value="{$profile.maximumWordLength}"></input>
                  <button type="submit" class="btn btn-primary mb-2" form="edit_profile_form">Submit</button>
                  <input type="reset" class="btn btn-light mb-2" form="edit_profile_form" id = "reset_edit_profile"></button>
        </div>
        <div class="col-8">

        </div>
    </div>
    <div class="container" >
    <div class="row" >
      <div class="col-sm">
          <h3>AMD Settings</h3>
          <table class="table-sm table-bordered table-hover table-responsive ">
              <thead  class="table-info text-center">
                <tr>

                  <!--
                  initialSilence - Is maximum initial silence duration before greeting.
                  If this is exceeded set as MACHINE
                  greeting - is the maximum length of a greeting.
                  If this is exceeded set as MACHINE
                  afterGreetingSilence - Is the silence after detecting a greeting.
                  If this is exceeded set as HUMAN
                  totalAnalysis Time - Is the maximum time allowed for the algorithm
                  to decide HUMAN or MACHINE
                  miniumWordLength - Is the minimum duration of Voice considered to be a word
                  betweenWordSilence - Is the minimum duration of silence after a word to consider the audio that follows to be a new word
                  maximumNumberOfWords - Is the maximum number of words in a greeting
                  If this is exceeded set as MACHINE
                  silenceThreshold - How long do we consider silence
                  maximumWordLength - Is the maximum duration of a word to accept.
                  If exceeded set as MACHINE
                  -->
                  <th scope="col">Action</th>
<!--                 <th scope="col">Active</th>-->
                  <th scope="col">id</th>
                  <th scope="col">name</th>
                  <th scope="col">initialSilence</th>
                  <th scope="col">greeting</th>
                  <th scope="col">afterGreetingSilence</th>
                  <th scope="col">totalAnalysis</th>
                  <th scope="col">miniumWordLength</th>
                  <th scope="col">betweenWordSilence</th>
                  <th scope="col">maximumNumberOfWords</th>
                  <th scope="col">silenceThreshold</th>
                  <th scope="col">maximumWordLength</th>
                  
              </tr>
              </thead>
              <tbody>
                  
                  {foreach $amd_settings as $setting}
                      <tr>
                        <td>
                            <button type="button" class="btn btn-warning edit_buttons"  data-action="edit_profile" data-id="{$setting.id}">EDIT</button>
                            <button type="button" class="btn btn-success export_buttons"  data-action="export_profile" data-id="{$setting.id}">EXPORT</button>

                        </div>
                        </td>
                          <!--<td>
                              <input type="checkbox" data-action="set_active_amd" data-id="{$setting.id}"
                              {if $setting.active == "1"}
                                 checked
                              {/if}
                              >
                              
                          </td>-->
                          <td>{$setting.id} </td>
                          <td class="set_fields_td">{$setting.name} 
                            <input class="set_fields_input" type="hidden" data-action="set_field" data-id = "{$setting.id}" data-field-name="name" value="{$setting.name}">
                          </td>
                          <td>{$setting.initialSilence}</td>
                          <td>{$setting.greeting}</td>
                          <td>{$setting.afterGreetingSilence}</td>
                          <td>{$setting.totalAnalysis}</td>
                          <td>{$setting.miniumWordLength}</td>
                          <td>{$setting.betweenWordSilence}</td>
                          <td>{$setting.maximumNumberOfWords}</td>
                          <td>{$setting.silenceThreshold}</td>
                          <td>{$setting.maximumWordLength}</td>

                         
                      </tr>
                      
                  {foreachelse}
                      <tr><td colspan="6" class="text-center"> No reports </td></tr>
                  {/foreach}
              </tbody>        
          </table>
      </div>   
      </div>
    </div>
  
  </main>
  <script>
    /*
    $('.set_fields_td').click(function(data) {
      //alert("a");
      $('.set_fields_input').attr("type","text");
      });

    $('.set_fields_input').keypress(function(data){
    if ( event.which == 13 ) {
     alert("entered" + $(this).data("action") +":" + $(this).data("id") +":" + $(this).data("field-name") );
    }
    });

    */
    $('#reset_edit_profile').click(function(data) {
      $('#edit_profile_form').submit();
    });

    $('.edit_buttons').click(function(data) {
      
        $('#edit_profile_id').val($(this).data("id"));
        //alert($(this).data("id"));
        $('#launch_edit_profile_form').submit();
    });

    $('.export_buttons').click(function(data) {
      
      $.post("",  { export_profile: $(this).data("id") } ).done(function(data){
           $('#show_export').toggle();
           $('#show_export_input').val(data);
            //html.body(data);
        });
  });
  </script>

