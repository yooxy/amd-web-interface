<!DOCTYPE html>
<html>
    
    <head>
        
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    {$refresh}
    <!-- plugins:css -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    
    </head>

    <body>
    <script src = "https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>

        
     
        <div  class="row" id="result_panel" style="display: none;">
            <div class="col-lg-10 " id = "result_panel_data" style="display: none;"> 

            </div>
            <div class="col-sm">
            <button type="button" class="btn btn-primary" id="btn_close_result">Close</button>
            </div>
        </div>
        {include file="{$file_to_load}"}

    </body>

</html>
