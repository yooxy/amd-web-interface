<?php
/* Smarty version 3.1.39, created on 2021-05-04 09:37:17
  from '/var/www/html/amd_handle/templates/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6090cf7d840775_55653709',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ee5464173abaf53104da17fcb0bec68d5a7f7bc9' => 
    array (
      0 => '/var/www/html/amd_handle/templates/page.tpl',
      1 => 1620103003,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6090cf7d840775_55653709 (Smarty_Internal_Template $_smarty_tpl) {
?><nav class="navbar navbar-dark bg-dark text-center">
    <a class="navbar-brand text-center" href="#"><?php echo $_smarty_tpl->tpl_vars['datetime']->value;?>
</a>
    <form class="form-inline" action="#" method="POST">
       <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="logout" value="yes">Logout</button>
    </form>
  </nav>
<div id="maincontainer" class="container" style="width: 75%;">
  <div class="row">
  <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
    <div class="sidebar-sticky pt-3">
      <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link active" href="index.php?main_page=amd_settings">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
            AMD Settings <span class="sr-only">(current)</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="index.php?main_page=amd_reports">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bar-chart-2"><line x1="18" y1="20" x2="18" y2="10"></line><line x1="12" y1="20" x2="12" y2="4"></line><line x1="6" y1="20" x2="6" y2="14"></line></svg>
            AMD Reports
          </a>
        </li>
      
      </ul>
    </div>
  </nav>
  
  <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['main_page']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
</div>
<?php }
}
