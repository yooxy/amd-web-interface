<?php
/* Smarty version 3.1.39, created on 2021-05-04 09:49:42
  from '/var/www/html/amd_handle/templates/main.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6090d266cd8ca9_83215914',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2cc665c753475146ff29ba1ea581e534e9f1749f' => 
    array (
      0 => '/var/www/html/amd_handle/templates/main.tpl',
      1 => 1620103621,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6090d266cd8ca9_83215914 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html>
    
    <head>
        
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php echo $_smarty_tpl->tpl_vars['refresh']->value;?>

    <!-- plugins:css -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    
    </head>

    <body>
    <?php echo '<script'; ?>
 src = "https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"><?php echo '</script'; ?>
>

        
     
        <div  class="row" id="result_panel" style="display: none;">
            <div class="col-lg-10 " id = "result_panel_data" style="display: none;"> 

            </div>
            <div class="col-sm">
            <button type="button" class="btn btn-primary" id="btn_close_result">Close</button>
            </div>
        </div>
        <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['file_to_load']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

    </body>

</html>
<?php }
}
