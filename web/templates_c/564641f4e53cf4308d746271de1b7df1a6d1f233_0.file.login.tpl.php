<?php
/* Smarty version 3.1.39, created on 2021-05-04 09:25:45
  from '/var/www/html/amd_handle/templates/login.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6090ccc9eebbb4_86417440',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '564641f4e53cf4308d746271de1b7df1a6d1f233' => 
    array (
      0 => '/var/www/html/amd_handle/templates/login.tpl',
      1 => 1620102343,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6090ccc9eebbb4_86417440 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auto-form-wrapper">
              <form action="#" method="POST">
                <div class="form-group">
                  <label class="label">Username</label>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Username" name="username">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="label">Password</label>
                  <div class="input-group">
                    <input type="password" class="form-control" placeholder="*********" name="password">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <button class="btn btn-primary submit-btn btn-block"> Login</button>
                </div>
                <div class="form-group d-flex justify-content-between">
                  <div class="form-check form-check-flat mt-0">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" checked> Keep me signed in </label>
                  </div>
                  <!--<a href="#" class="text-small forgot-password text-black">Forgot Password</a>-->
                </div><!--
                <div class="text-block text-center my-3">
                  <span class="text-small font-weight-semibold">Not a member ?</span>
                  <a href="register.html" class="text-black text-small">Create new account</a>
                </div>-->
              </form>
            </div>
            <!--<ul class="auth-footer">
              <li>
                <a href="#">Conditions</a>
              </li>
              <li>
                <a href="#">Help</a>
              </li>
              <li>
                <a href="#">Terms</a>
              </li>
            </ul>
              -->
            
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js 
  <?php echo '<script'; ?>
 src="assets/vendors/js/vendor.bundle.base.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="assets/vendors/js/vendor.bundle.addons.js"><?php echo '</script'; ?>
> -->
  <!-- endinject -->
  <!-- inject:js 
  <?php echo '<script'; ?>
 src="assets/js/shared/off-canvas.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="assets/js/shared/misc.js"><?php echo '</script'; ?>
> -->
  <!-- endinject -->
  <!--<?php echo '<script'; ?>
 src="assets/js/shared/jquery.cookie.js" type="text/javascript"><?php echo '</script'; ?>
>-->
</body>

<?php }
}
