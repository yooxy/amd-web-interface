<?php
/* Smarty version 3.1.39, created on 2021-05-04 09:56:00
  from '/var/www/html/amd_handle/templates/amd_reports.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6090d3e01b0ac6_09199027',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c6d0ce88d7bd96787c1f3d44e810db4630ec9c0c' => 
    array (
      0 => '/var/www/html/amd_handle/templates/amd_reports.tpl',
      1 => 1620104158,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6090d3e01b0ac6_09199027 (Smarty_Internal_Template $_smarty_tpl) {
?><main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <h1 class="h2">AMD Reports</h1>
      <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
          <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
          <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
        </div>
        <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
          This week
        </button>
      </div>
    </div>

    <div class="row" >
      <div class="col-sm">
          <h3>Reports</h3>
          <table class="table-sm table-bordered table-hover ">
              <thead  class="table-info text-center">
                <tr>
                   
                  <th scope="col">id</th>
                  <th scope="col">unqueid</th>
                  <th scope="col">calldate</th>
                  <th scope="col">AMD Status</th>
                  <th scope="col">AMD Cause</th>
                  
                  
              </tr>
              </thead>
              <tbody>
                  
                  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['reports']->value, 'report');
$_smarty_tpl->tpl_vars['report']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['report']->value) {
$_smarty_tpl->tpl_vars['report']->do_else = false;
?>
                      <tr>
                          
                          <td><?php echo $_smarty_tpl->tpl_vars['report']->value['id'];?>
 </td>
                          <td><?php echo $_smarty_tpl->tpl_vars['report']->value['uniqueid'];?>
</td>
                          <td><?php echo $_smarty_tpl->tpl_vars['report']->value['calldate'];?>
</td>
                          <td><?php echo $_smarty_tpl->tpl_vars['report']->value['amdstatus'];?>
 </td>
                          <td><?php echo $_smarty_tpl->tpl_vars['report']->value['amdcause'];?>
 <?php echo $_smarty_tpl->tpl_vars['report']->value['amdcause_ext'];?>
</td>
                          <!--(r:<?php echo $_smarty_tpl->tpl_vars['calls']->value[$_smarty_tpl->tpl_vars['report']->value['id']]['client_rateid'];?>
 o:<?php echo $_smarty_tpl->tpl_vars['calls']->value[$_smarty_tpl->tpl_vars['report']->value['id']]['client_optionid'];?>
)-->
                      </tr>
                      
                  <?php
}
if ($_smarty_tpl->tpl_vars['report']->do_else) {
?>
                      <tr><td colspan="6" class="text-center"> No reports </td></tr>
                  <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
              </tbody>        
          </table>
      </div>   
      </div>
      
  
  </main>
  

<?php }
}
